# Change Log
All notable changes to this project will be documented in this file.

This project adheres to [Semantic Versioning](http://semver.org/).

This changelog adheres to [keepAChangeLog](http://keepachangelog.com/).

## [1.0.4] - 2016-12-01
### Added
- RabbitMq link
- OPS team link
- Random stuff

## [1.0.0] - 2016-11-24
### Added
- everything
