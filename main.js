"use strict";
(($)=>{
	const SOURCES_URL		=  'data/sources.json' //
		, SOURCES_PREFIX	=  "https://bitbucket.org/damien_benoit/ccmlink/raw/master/"
		// , SOURCES_PREFIX	=  ""
		, UPDATE_FREQUENCY	= 1e3 * 60 * 60 * 2
		;
	let $description		= $('.description')
		, $linksContainer	= $('#linkContainer')
		, $hiddenContainer	= $('#hiddenContainer')
		, $sources			= $('#sources')
		, $hiddenZone		= $('.hiddenZone')
		, $footer			= $('.footer')
		, $settingsButton	= $('.settingsButton')
		, $lastUpdateDate	= $('.lastUpdateDate')
		, $saveLinksButton	= $('.saveLinksButton')
		, $myLinks			= $('#myLinks')
		, storage			= chrome.storage.sync || chrome.storage.local // on ffox sync storage is not available, using local instead.
		, _data				= {
			linksOrder		: []
			,hiddenOrder	: []
			,links 			: {}
			,sources		: []
			,lastUpdate		: 0
			,subscription	: []
			,myLinks		: ''
		}
		, getLinks = () =>
		{
			return getDataFromAjax(SOURCES_PREFIX+SOURCES_URL)
				.then( sources =>
				{
					_data.sources = sources.filter( name => name.indexOf('empty') === -1 );
					// console.info('loading from,', sources);
					return Promise.all( sources.map(
						source => {
							// console.log(source);
							// adding source only if subscribed :
							// return getDataFromAjax(source);
							if (_data.subscription.indexOf(source) !== -1 ){ return getDataFromAjax(SOURCES_PREFIX+source);}
						})
					)
						.then(data => {
							// console.log("data",data);
							let links = {};
							data.forEach( linkSource =>
							{
								for(let link in linkSource ){
									links[link] = linkSource[link];
								}
							});
							return links;
						})
					;
				});
		}
		, getDataFromAjax	= source => {
			return new Promise( (resolve/*,reject*/) =>
			{
				$.ajax(
					{
						dataType : "json"
						, url : source
						, success: data =>
						{
							console.log(data);
							resolve(data);
						}
						, error : e => { console.log('%c error','color:red',e); resolve({}); }
					}
				);
			});
		}
		, getOrder			= () => {
			return new Promise( (resolve/*,reject*/) =>
			{
				storage.get("linksOrder", order =>
				{
					// console.log(order);
					if( order && order.linksOrder && $.isArray(order.linksOrder) && order.linksOrder.length)
					{
						// console.log('using synchronized order',order.linksOrder);
						resolve(order.linksOrder);
					}
					else
					{
						// console.log("using default order");
						getLinks()
							.then( links => resolve(Object.keys(links)) );
					}
				});

			});
		}
		, getHiddenOrder	= () => {
			return new Promise( (resolve/*,reject*/) =>
			{
				storage.get("hiddenOrder", order =>
				{
					// console.log(order);
					if( order && order.hiddenOrder && $.isArray(order.hiddenOrder) && order.hiddenOrder.length)
					{
						// console.log('using synchronized order',order.hiddenOrder);
						resolve(order.hiddenOrder);
					}
					else
					{
						// console.log("using default order");
						resolve([]);
					}
				});

			});
		}
		, getOrderFrom$Node = $node => { let order = []; $node.find('li a').each( (i,node) => order.push($(node).attr('id')) ); return order; }
		, updateOrder = () =>
		{
			_data.linksOrder	= getOrderFrom$Node($linksContainer);
			_data.hiddenOrder	= getOrderFrom$Node($hiddenContainer);
			// console.log(_data);
			saveData();
		}
		, saveData = () =>
		{
			// console.info('Data saved',_data);
			storage.set(_data);
		}
		, getIconFromLink = (linkName,link) => `<li><a class="icon" id="${linkName}" target="_blank" href="${link.url}"><img src="${link.icon}">${ link.subIcon ? `<img src="${link.subIcon}" class="secondary">` : '' }<div class="legend">${linkName} ${link.description ? ' - '+link.description : ''}</div></a></li>`
		, getStringFromLinksArray = linksArray =>
		{
			// console.log("la",linksArray);
			let nodes = '';
			linksArray.forEach( linkName =>
			{
				// console.log(linkName)
				let link = _data.links[linkName];
				if(typeof link === "object")
				{
					// console.log('>>',linkName);
					nodes += getIconFromLink(linkName,link);
				}
				// else
				// {
				// 	console.info(linkName,'removed from real list, removing it from ordered list');
				// 	linksArray.splice(linksArray.indexOf(linkName),1);
				// 	saveData();
				// }
			});
			return nodes;
		}
		, showHiddenZone = ()=>
		{
			$hiddenZone.addClass('visible');
			$settingsButton.html('Hide Settings');
		}
		, hideHiddenZone = ()=>
		{
			$hiddenZone.removeClass('visible');
			$settingsButton.html('Show Settings');
		}
		, toggleHiddenZone = () => {
			$hiddenZone.hasClass('visible') ? hideHiddenZone() : showHiddenZone();
		}
		, getDataFromStorage = dataName => {
			return new Promise( (resolve/*,reject*/) =>
			{
				storage.get(dataName, data => { resolve(data); } );
			});
		}
		, getStoredData = () =>
		{
			return Promise.all(
				[
					getDataFromStorage("linksOrder")
					,getDataFromStorage("hiddenOrder")
					,getDataFromStorage("links")
					,getDataFromStorage("sources")
					,getDataFromStorage("lastUpdate")
					,getDataFromStorage("subscription")
					,getDataFromStorage("myLinks")
				]
			)
				.then( data =>
				{
					return {
						linksOrder		: data[0].linksOrder || []
						,hiddenOrder	: data[1].hiddenOrder || []
						,links			: data[2].links || {}
						,sources		: Array.isArray(data[3].sources) ? data[3].sources.filter( name => name.indexOf('empty') === -1 ) : []
						,lastUpdate		: data[4].lastUpdate || 0
						,subscription	: data[5].subscription || ["https://bitbucket.org/damien_benoit/ccmlink/raw/master/data/default.links.json"]
						,myLinks		: data[6].myLinks || ` {  "myCustomExampleLink": {
								"url": "http://random.cat/"
								,"icon": "icons/happy.png"
								,"subIcon": "icons/sad.png"
								,"description": " random cat"
							} }`
					};
				}
			);
		}
		, getDistantDatas = () =>
		{
			// console.log('getting distant data');
			return Promise.all(
				[
					getOrder()
					, getHiddenOrder()
					, getLinks()
				])
				.then( saved_data =>
				{
					return {
						linksOrder		: saved_data[0]
						, hiddenOrder	: saved_data[1]
						, links			: saved_data[2]
						, lastUpdate	: new Date().getTime()
					};
				})
				.then( data => {
					// console.log(data);
					Object.assign(_data,data);
					saveData();
					return _data;
				})
				;
		}
		, addMyLinks = data =>
		{
			// console.info(JSON.parse(_data.myLinks));
			Object.assign(data.links,JSON.parse(_data.myLinks));
			return data;
		}
		, getDatas = () =>
		{
			return new Promise( (resolve/*,reject*/) =>
			{
				getStoredData()
					.then( data =>
					{
						Object.assign(_data,data);
						// console.log('ddd',data);
						let now = new Date().getTime();
						// console.log(now,data.lastUpdate,now-data.lastUpdate,UPDATE_FREQUENCY);
						if(data.lastUpdate === 0 ||  (now-data.lastUpdate) > UPDATE_FREQUENCY )
						{
							getDistantDatas()
								.then(resolve);
						}
						resolve(data);
					});
			});
		}
		, withZero = unit => ("0" + unit).slice(-2)
		, getSimplifiedDate = date =>
		{
			date = new Date(date);
			return `${ withZero(date.getDate()) }/${ withZero(date.getMonth()+1) } ${ withZero(date.getHours()) }:${ withZero(date.getMinutes()) }:${ withZero(date.getSeconds()) }`;
		}
		, bindBehavior	= () =>
		{
			let hiddenTimer 	= 0
				, checkMyLinksValidity = () =>
				{
					try {
						JSON.parse($myLinks.val());
						$saveLinksButton.html('Save').addClass('btn-primary').removeClass('btn-warning');
						return true;
					} catch (e) {
						$saveLinksButton.html('Save (Invalid JSON)').removeClass('btn-primary').addClass('btn-warning');
						return false;
					}
				}
				;

			$("body")
				.on('mouseenter','.icon',function()
				{
					let $this = $(this);
					$description.html($this.find('.legend').html()).removeClass('fadeOut');
					$footer.html($this.attr('href')).removeClass('fadeOut');
				})
				.on('mouseleave','.icon',() =>
				{
					$description.addClass('fadeOut');
					$footer.addClass('fadeOut');
				})
				;
			$settingsButton.on('click',toggleHiddenZone );
			$('.container')
				.sortable(
				{
					update : updateOrder
					,connectWith: "ul.container"
				}
				)
				.disableSelection()
				.on('sort',function()
				{
					showHiddenZone();
					clearTimeout(hiddenTimer);
					hiddenTimer = setTimeout(hideHiddenZone,2e3);
				});
				// .on('stop',function(){ console.log('stopped');})

				// chrome.browserAction.onClicked.addListener(()=>{
				// 	console.log('changing icon');
				// 	chrome.browserAction.setIcon({path:'icons/logo.ccm.revert.png'});
				// });
			$('.refreshButton').on('click',refresh);
			$sources.on('click','input',function()
			{
				// console.log('refreshing');
				let $this = $(this)
					, val = $this.val()
					;
				if($this.prop('checked'))
				{
					// adding it
					_data.subscription.indexOf(val) === -1 &&  _data.subscription.push(val);
				}
				else {
					//removing it
					_data.subscription.splice(_data.subscription.indexOf(val),1);
				}
				saveData();
				refresh();
			});
			$saveLinksButton
				.on('click',function(){
					if(checkMyLinksValidity())
					{
						// console.log('saving');
						_data.myLinks = JSON.stringify(JSON.parse($myLinks.val()));
						$saveLinksButton.html('Saved !').addClass('btn-success').removeClass('btn-warning btn-primary');
						setTimeout(()=>$saveLinksButton.html('Save').addClass('btn-primary').removeClass('btn-success'), 3e3 );
						saveData();
						refresh();
					}
				});
			$myLinks.on('keyup',checkMyLinksValidity);

			return Promise.resolve();
		}
		, refresh = () =>
			getDistantDatas()
				.then(addMyLinks)
				.then(renderContent)
				.catch(e=>console.log("err",e))
		, renderContent = data =>
		{
			// console.log("before Render",_data,data);
			let nodes			= getStringFromLinksArray(data.linksOrder)
				, sources		= ''
				;
			// console.log(data);
			// now rotating over real source if ever there is new entry
			Object.keys(data.links).forEach( linkName => {
				// console.log(linkName)
				let link = data.links[linkName];
				// if not allready included :
				if(data.linksOrder.indexOf(linkName) === -1 && data.hiddenOrder.indexOf(linkName) === -1)
				{
					// console.log('reinjecting',linkName);
					nodes += getIconFromLink(linkName,link);
				}
			});
			_data.sources.forEach( source => {
				sources += `<li><label><input type="checkbox" value="${source}" ${ _data.subscription.indexOf(source) !== -1 ? 'checked=checked' : '' }> ${source.split(/\.links\.json/)[0].split(/data\//)[1]}</label></li>`;
			});
			$linksContainer.empty().append(nodes);
			$hiddenContainer.empty().append(getStringFromLinksArray(data.hiddenOrder));
			$('img').each(function()
			{
				this.onerror = () => { this.src = "icons/logo.ccm.128.png"; };
			});
			$sources.empty().append(sources);
			$lastUpdateDate.html(getSimplifiedDate(_data.lastUpdate));
			$myLinks.html((()=>{
				let json = '';
				try {
					json = JSON.stringify(JSON.parse(_data.myLinks),null,'\t');
				} catch (e) {
					json = _data.myLinks;
				}
				return json;
			})());
			return Promise.resolve();
		}
		;

// main :
	getDatas()
		.then(addMyLinks)
		.then(renderContent)
		.then(bindBehavior)
		;

})(jQuery);
